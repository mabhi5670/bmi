import styles from "./Button.module.css";

export const Button = ({name,clickhandler}) =>{
    return <button className={styles.btn} onClick = {clickhandler}>{name}</button>
}
