import { useEffect, useState } from "react";
import Box from "../Box/Box";
import Measurement from "../measurement/Measurement";
import styles from "./Main.module.css";


const Main = () =>{

    const[height,setHeight] = useState(5.0);
    const[weight,setWeight] = useState(59);
    const[result,setResult] = useState(0);

    const Increment = (state,func,val) => {
        let data = state+val;
        data = parseFloat(data.toFixed(1));
        func(data);
    }

    const Decrement = (state,func,val) => {
        let data = state-val;
        data = parseFloat(data.toFixed(1))
        func(data);
    }

    useEffect(() => {
        const data = 2 * weight/height;
        setResult(data.toFixed(2));
    }, [height,weight])

    return(
        <main className={styles.main}>

            <section className={styles.row}>
                <Measurement title="Height" value={height} Increment={() => Increment(height,setHeight,0.1)} Decrement={() => Decrement(height,setHeight,0.1)}/>
                <Measurement title="Weight" value={weight} Increment={() => Increment(weight,setWeight,1)} Decrement={() => Decrement(weight,setWeight,1)}/>
            </section>

            <section className={styles.row}>
                <Box>{result}</Box>
            </section>

            <section className={styles.row}>
                <Box>{
                        result< 18 ? "underweight" :
                        result < 25 ? "normal":
                        result < 30 ? "overweight":
                        result >= 30 ? "obese": null
                    
                    }</Box>
            </section>

        </main>
    )
}

export default Main;
