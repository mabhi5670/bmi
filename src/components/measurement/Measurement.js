import Box from "../Box/Box";
import { Button } from "../Button/Button";
import styles from "./Measurement.module.css";


const Measurement = ({title,value,Increment,Decrement}) => {
    return(
        <div className={styles.container}>
            <Box>
                <h2>{title}</h2>
                <h3>{value}</h3>
                <div className={styles.row}>
                    <Button name="+" clickhandler={Increment}/>
                    <Button name="-" clickhandler={Decrement}/>
                </div>
            </Box>
        </div>
    );
}

export default Measurement;
